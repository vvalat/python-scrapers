"""
This script allows downloading chapters of a manga on the referenced site through scraping,
async downloading and packaging. Updated, refactored and improved from previous iteration.
"""
import os
import re
import time
import zipfile
import threading
from typing import List, Tuple, Dict
from urllib.parse import urljoin

import asyncio
import requests
import bs4
from aiofiles import open as async_open
from aiofiles.os import remove as async_remove
from aiohttp import ClientSession, TCPConnector

from js import process as unpack_js, JSUnpackerError


TEMP_DIR = os.path.join(os.getcwd(), "tmp")
OUT_DIR = os.path.join(os.getcwd(), "out")

MAX_QUERY_REQUESTS = 10
MAX_DL_REQUESTS = 4

# Base values for script requests (the higher, the longer... but safer)
TIME_TO_NEXT_TRY = 5
MAX_RETRIES = 15

Lock = threading.Lock()
DOWNLOADED_CHAPTERS = 0
TOTAL_CHAPTERS = 0

# RegEx compilation
COUNT_RE = re.compile(r"(?<=var imagecount)\s*=\s*\d+")
CHAPTER_ID_RE = re.compile(r"(?<=var chapterid)\s*=\s*\d+")
ADDITIONAL_CHAPTER_RE = re.compile(r"(/c\d+)\.(\d+)/")
PACKAGING_RE = {
    "no_volume": re.compile(r"manga/\w+/c(\d+\w)/"),
    "volume_pending": re.compile(r"manga/\w+/vTBD/c(\d+\w)/"),
    "volume": re.compile(r"manga/\w+/v(\d+)/c(\d+\w)/"),
}
KEY_SCRIPT_RE = re.compile(r"guidkey=([\\\'\"+\w]*);\$")
IMG_SCRIPT_RE = {
    "domain": re.compile(r"pix=\"([^\"]+)"),
    "path": re.compile(r"pvalue=\[\"([^\"]+)"),
}
DELIMITER_REMOVAL_RE = re.compile(r"[+\"']")
IMG_EXTENSION_RE = re.compile(r"/[\w~-]+\.(\w+)\?")


def get_first_pages(response: requests.Response) -> Tuple[str, List[str]]:
    """
    Scraps the title page for all leading chapter pages it contains
    Note: If the site changes with a new DOM, this will likely be the first thing to test
    :param response: The response object from the initial input
    :return: a list of URLs
    """
    parser = bs4.BeautifulSoup(response.text, "html.parser")
    try:
        manga_title = parser.select("span.detail-info-right-title-font")[0].text
    except (AttributeError, IndexError):
        manga_title = "Unknown Title"
    choices = len(parser.select("div[id^=list-]"))
    if choices == 2:
        try:
            by_chapter = parser.select("div#list-1")[0].select("li:has(a > div > p.title3) > a")
            by_volume = parser.select("div#list-2")[0].select("li:has(a > div > p.title3) > a")
        except (IndexError, TypeError, AttributeError):
            return manga_title, []
        user_input = None
        while user_input not in ("1", "2"):
            user_input = input(
                f"You can select the releases by chapter ({len(by_chapter)} items) or "
                f"by volume ({len(by_volume)} items)\n1: by chapter\n2: by volume\n"
            )
        selection = by_chapter if user_input == "1" else by_volume
    elif choices == 1:
        selection = parser.select("li:has(a > div > p.title3) > a")
    else:
        return manga_title, []
    return manga_title, [urljoin(response.url, link.attrs["href"]) for link in selection]


def get_packaging_info(chapter_url: str) -> Tuple[str, str]:
    """
    Retrieves chapter and volume information from URL
    Note: Information could be passed down from top level but this is safer
    Known URL path patterns:
        - /title/c[0-9]{3}/page.html             No volume information
        - /title/vTBD/c[0-9]{3}/page.html        Volume information pending
        - /title/v[0-9]{2}/c[0-9]{3}/page.html   Volume information existing
    """
    # Intermediate or bonus (omake) chapters can have a dot and then a second number
    def convert_number(match_obj: re.Match):
        """
        Converts a number to the corresponding letter of the alphabet by rank
        Example: title/c103.5/2.html  ->  title/c103e/2.html
        """
        return f"{match_obj.group(1)}{chr(96 + int(match_obj.group(2)))}/"

    chapter_url = re.sub(ADDITIONAL_CHAPTER_RE, convert_number, chapter_url)

    volume = chapter = ""
    for name, pattern in PACKAGING_RE.items():
        if not (match := re.search(pattern, chapter_url)):
            continue
        if name in ("no_volume", "volume_pending"):
            chapter = match.group(1)
        elif name == "volume":
            volume, chapter = match.groups()
        break
    return volume, chapter


async def get_chapter_links(chapter_url: str, async_session: ClientSession) -> Dict[Tuple, List]:
    """
    Retrieves basic html information about the chapter and builds URLs from them
    """
    async with async_session.get(chapter_url) as response:
        html = await response.text()

    # Navigation is generated in JS but retro-engineering it, we find the total count and ID
    count_search = re.search(COUNT_RE, html)
    chapter_id_search = re.search(CHAPTER_ID_RE, html)
    if not count_search or not chapter_id_search:
        raise Exception(f"Cannot retrieve information on the chapter at {chapter_url}")
    try:
        image_count = count_search.group().replace(" ", "").split("=")[1]
        chapter_id = chapter_id_search.group().replace(" ", "").split("=")[1]
        assert image_count.isnumeric() and chapter_id.isnumeric()
    except (IndexError, AssertionError):
        print(f"Error parsing this page: {chapter_url}. Check for `var csshost` in the DOM")
        raise
    # URLs are generated automatically so it is safe to deduce them
    return {
        (chapter_id, *get_packaging_info(chapter_url)): [
            (str(i), urljoin(chapter_url, f"{i}.html")) for i in range(1, int(image_count) + 1)
        ]
    }


async def process_pages(url_list: List[str]) -> List[Dict[Tuple, List]]:
    async with ClientSession(connector=TCPConnector(limit=MAX_QUERY_REQUESTS)) as async_session:
        async_session.cookie_jar.update_cookies({"isAdult": "1"})
        page_tasks, _ = await asyncio.wait(
            [get_chapter_links(url, async_session) for url in url_list]
        )
        return [task.result() for task in page_tasks]


async def retrieve_and_decode_script(async_session, page: str, page_url: str, chapter_id: str):
    """
    The image link is "hidden" in a packed JS script that is requested by the page URL. This request
    needs a key that is itself in a script in the page URL. We will retrieve that and return what
    is needed to make the same request.
    :param async_session: Asynchronous session shared for cookie purposes
    :param page: The page number
    :param page_url: The URL for the page
    :param chapter_id: The unique ID for the current chapter
    :return: tuple: (url:str, query_params:dict, page: str)
    """
    async with async_session.get(page_url) as response:
        html = await response.text()
    parser = bs4.BeautifulSoup(html, "html.parser")

    # Find the script in the page
    def detect_key_script(tag: bs4.Tag) -> bool:
        return tag.contents and "dm5_key" in tag.contents[0]

    script = parser.find(detect_key_script)

    # Decode it
    try:
        decoded = re.search(KEY_SCRIPT_RE, unpack_js(script.contents[0])).group(1)
        key = re.sub(DELIMITER_REMOVAL_RE, "", decoded)
    except (JSUnpackerError, TypeError, AttributeError):
        print(f"Something went wrong trying to get the key for {page_url}")
        raise

    return urljoin(page_url, "chapterfun.ashx"), {"cid": chapter_id, "page": page, "key": key}, page


async def get_script_urls(
    async_session: ClientSession, pages: List[Tuple], chapter_id: str, vol: str, chapter: str
):
    """
    :param async_session: Asynchronous session shared for cookie purposes
    :param pages: a list of tuples containing the page number and the corresponding URL
    :param chapter_id: The unique ID for the current chapter
    :param vol: The current volume number (an empty string if no information found)
    :param chapter: The current chapter number
    :return: the same dict with complete URLs in the list value
    """
    url_tasks, _ = await asyncio.wait(
        [
            retrieve_and_decode_script(async_session, page, page_url, chapter_id)
            for page, page_url in pages
        ]
    )
    return {(chapter_id, vol, chapter): [url_task.result() for url_task in url_tasks]}


async def get_image_script_urls(chapter_dicts: List[dict]):
    async with ClientSession(connector=TCPConnector(limit=MAX_QUERY_REQUESTS)) as async_session:
        async_session.cookie_jar.update_cookies({"isAdult": "1"})
        script_tasks, _ = await asyncio.wait(
            [
                get_script_urls(async_session, page_urls, *chapter_info)
                for chapter in chapter_dicts
                for chapter_info, page_urls in chapter.items()
            ]
        )
        return [task.result() for task in script_tasks]


async def get_image(url: str, page: str, file_prefix: str, dl_session: ClientSession):
    async with dl_session.get(url) as image:
        if image.status == 200:
            extension = re.search(IMG_EXTENSION_RE, url).group(1)
            path = os.path.join(TEMP_DIR, f"{file_prefix}_P{page:0>3}.{extension}")
            async with async_open(path, "wb") as file:
                await file.write(await image.read())
            return path
        print(f"Error while downloading {url}")


async def package_chapter(url_list: List[Tuple[str, str]], _, volume: str, chapter: str):
    """
    Launch the retrieval of the images and package in an archive
    """
    global DOWNLOADED_CHAPTERS, TOTAL_CHAPTERS

    file_prefix = f"{f'Volume {volume} - ' if volume else ''}Chapter {chapter}"
    async with ClientSession(connector=TCPConnector(limit=MAX_DL_REQUESTS)) as dl_session:
        file_dl_tasks, _ = await asyncio.wait(
            [get_image(url, page, file_prefix, dl_session) for url, page in url_list]
        )
    file_names = [file_dl_task.result() for file_dl_task in file_dl_tasks if file_dl_task.result()]

    # There is no asynchronous zipfile equivalent but the compression is at lowest so should be OK
    archive_name = f"{file_prefix}.cbz"  # e-Reader format (equivalent to zip)
    with zipfile.ZipFile(os.path.join(archive_directory, archive_name), "w") as archive:
        for file in file_names:
            archive.write(file)

    with Lock:
        DOWNLOADED_CHAPTERS += 1
    print(f"{archive_name} was written, {DOWNLOADED_CHAPTERS}/{TOTAL_CHAPTERS}")
    for file in file_names:
        await async_remove(file)


def retrieve_images_urls(script_data: List[dict]):
    """
    Due to cloudflare protection, asynchronous requests in parallel have bad results so we take the
    longer but safer road.
    """
    sync_session = requests.session()
    sync_session.get(main_url)
    for chapter_scripts in script_data:
        for chapter_data, script_url_data in chapter_scripts.items():
            url_list = []
            for position, (url, query_params, page) in enumerate(script_url_data):
                response = sync_session.get(url, params=query_params)
                count = 0
                while response.status_code != 200 or not response.text:
                    print(
                        f"({count}/{MAX_RETRIES}) Retrying {response.request.url.split('/')[-1]} "
                        f"({position}/{len(script_url_data)}) in {TIME_TO_NEXT_TRY} s.",
                        end="\r",
                    )
                    if count > MAX_RETRIES:
                        print("\nThe server seems to be rejecting our requests")
                        raise Exception("Timeout reached")
                    time.sleep(TIME_TO_NEXT_TRY)
                    response = sync_session.get(url, params=query_params)
                    count += 1
                parser = bs4.BeautifulSoup(response.text, "html.parser")
                decoded = ""
                try:
                    decoded = unpack_js(parser.text)
                    image_url = (
                        f"https:{re.search(IMG_SCRIPT_RE['domain'], decoded).group(1)}"
                        f"{re.search(IMG_SCRIPT_RE['path'], decoded).group(1)}"
                    )
                except (JSUnpackerError, TypeError, AttributeError):
                    print(f"Error while trying to decode the script for {url}\nReceived: {decoded}")
                    raise
                url_list.append((image_url, page))
                time.sleep(TIME_TO_NEXT_TRY)
            asyncio.run(package_chapter(url_list, *chapter_data))


if __name__ == "__main__":
    urls = []
    message = title = ""
    try:
        main_url = input("Enter/Paste the parent page URL, usually ending with '/manga/<title>'\n")
        session = requests.Session()
        session.cookies.set("isAdult", "1")  # Totally discovered this by chance, I swear...
        res = session.get(main_url)
        assert res.status_code == 200
        title, urls = get_first_pages(res)
    except requests.exceptions.RequestException:
        message = "Invalid URL"
    except AssertionError:
        message = "The URL could not be reached"
    if not urls:
        print(f"Nothing found: {message or 'Unknown problem, maybe the site has been updated...'}")
        exit(1)
    TOTAL_CHAPTERS = len(urls)
    chapters_info = asyncio.run(process_pages(urls))
    archive_directory = os.path.join(OUT_DIR, title)
    for directory in (TEMP_DIR, OUT_DIR, archive_directory):
        if not os.path.isdir(directory):
            os.mkdir(directory)
    script_urls = asyncio.run(get_image_script_urls(chapters_info))
    retrieve_images_urls(script_urls)
