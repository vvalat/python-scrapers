# Python Scrapers

Various python projects revolving around scraping

### Contents (by chronological order of conception):

###### manga_fox (fanfox.net) _(updated from 2017 initial project)_
This script's goal is to download a number of images corresponding to a chapter
of a manga and package them according to available information during scraping.

Due to a more complex (and "secure" coding of the site), an update was needed
including deobfuscating javascript and some other tricks to remain clientless.
I took the opportunity to make this asynchronous as possible.

Possible improvements:
- Allowing inputting a chapter reference to prevent downloading already acquired
  chapters
- Gathering chapters from the same volume in a single archive

###### tubbz
A very straightforward (and single-use) script to download specific images on an
e-commerce site for rubber duckies' aficionados.