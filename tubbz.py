"""
Tubbz is a brand of rubber ducks for the bath inspired by pop culture references.
This script downloads for each product page the main photo (supposedly a three-quarter front view)
Note: Uses async request lib because of the number of files to download (100+)
"""
import os
from pathlib import Path

import asyncio
import requests
import aiofiles
from aiohttp import ClientSession, TCPConnector
from bs4 import BeautifulSoup


PICTURES_DIR = os.path.join(Path.home(), "Pictures")
PRODUCT_PAGE = "http://www.numskull.com/tubbz/"
CSS_CLASS = {"main": "allCategoryWiseProductscolumn", "product": "singleProductImageSliderWrap"}
# Avoid 503 errors and be nice to the site
MAX_REQUESTS = 5


async def retrieve_image_url(session, product_url):
    async with session.get(product_url) as response:
        return await response.text()


async def process_pages(url_list):
    async with ClientSession(connector=TCPConnector(limit=MAX_REQUESTS)) as session:
        completed_tasks, _ = await asyncio.wait(
            [retrieve_image_url(session, link) for link in url_list]
        )
        return [task.result() for task in completed_tasks]


async def download_image(img_url):
    async with ClientSession(connector=TCPConnector(limit=MAX_REQUESTS)) as session:
        print(img_url, type(img_url))
        async with session.get(img_url) as response:
            if response.status == 200:
                file = await aiofiles.open(
                    os.path.join(PICTURES_DIR, "Tubbz", img_url.split("/")[-1]), "wb"  # binary file
                )
                await file.write(await response.read())
                await file.close()


async def process_images(url_list):
    await asyncio.wait([download_image(img_url) for img_url in url_list])


if __name__ == "__main__":
    if not os.path.isdir(os.path.join(PICTURES_DIR, "Tubbz")):
        if not os.path.isdir(PICTURES_DIR):
            os.mkdir(PICTURES_DIR)
        os.mkdir(os.path.join(PICTURES_DIR, "Tubbz"))
    parser = BeautifulSoup(requests.get(PRODUCT_PAGE).text, features="html.parser")
    urls = [em.find("a")["href"] for em in parser.find_all("div", class_=CSS_CLASS["main"])]
    responses = asyncio.run(process_pages(urls))
    image_urls = [
        BeautifulSoup(res, features="html.parser")
        .find("div", class_=CSS_CLASS["product"])
        .find("img")["src"]
        for res in responses
    ]
    asyncio.run(process_images(image_urls))
